package com.jolybell.repeattaskwsr

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jolybell.repeattaskwsr.databinding.ItemBinding
import kotlin.random.Random

class Adapter(val count: Int): RecyclerView.Adapter<Adapter.ViewHolder>() {
    class ViewHolder(binding: ItemBinding): RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemBinding.inflate(LayoutInflater.from(parent.context), parent,false))
    }

    override fun getItemCount(): Int = count

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

    }
}