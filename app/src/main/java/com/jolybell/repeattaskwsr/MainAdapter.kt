package com.jolybell.repeattaskwsr

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jolybell.repeattaskwsr.databinding.ItemMainBinding

class MainAdapter(val list: List<Int>): RecyclerView.Adapter<MainAdapter.ViewHolder>() {
    class ViewHolder(val binding: ItemMainBinding): RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemMainBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.binding.bottom.visibility = View.VISIBLE
        holder.binding.top.visibility = View.VISIBLE
        holder.binding.bottom.setImageDrawable(ContextCompat.getDrawable(holder.itemView.context, R.drawable.bottom))
        holder.binding.top.setImageDrawable(ContextCompat.getDrawable(holder.itemView.context, R.drawable.top))

        if (itemCount != 1) {
            if (position == 0)
                holder.binding.bottom.visibility = View.GONE
            else if (position == itemCount - 1)
                holder.binding.top.setImageDrawable(
                    ContextCompat.getDrawable(
                        holder.itemView.context,
                        R.drawable.check
                    )
                )
            else {
                holder.binding.top.setImageDrawable(
                    ContextCompat.getDrawable(
                        holder.itemView.context,
                        R.drawable.check
                    )
                )
                holder.binding.bottom.visibility = View.GONE
            }
        }

        holder.binding.rec.adapter = Adapter(list[position])
        holder.binding.rec.layoutManager = LinearLayoutManager(holder.itemView.context)
    }


}