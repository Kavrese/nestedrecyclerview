package com.jolybell.repeattaskwsr

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.jolybell.repeattaskwsr.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val list = listOf(10, 2 ,5, 1, 2, 3, 6, 1, 2, 4, 9)
        binding.recMain.adapter = MainAdapter(list)
        binding.recMain.layoutManager = LinearLayoutManager(this)
    }
}